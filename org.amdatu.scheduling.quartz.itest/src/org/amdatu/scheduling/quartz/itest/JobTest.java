/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.scheduling.quartz.itest;

import java.util.concurrent.TimeUnit;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import junit.framework.TestCase;

import org.amdatu.scheduling.quartz.example.api.MessageStore;

public class JobTest extends TestCase{
	private volatile MessageStore m_messageStore;
	
	@Override
	public void setUp() throws Exception {
		configure(this).add(createServiceDependency().setService(MessageStore.class)).apply();
	}
	
	public void testJobExecuted() throws InterruptedException {
		//Give the jobs some time to execute
		TimeUnit.SECONDS.sleep(3);
		
		assertEquals(6, m_messageStore.getNrOfMessages("[ANNOTATIONS JOB]"));
		assertEquals(11, m_messageStore.getNrOfMessages("[PROPERTIES JOB]"));
	}
	
    @Override
    public void tearDown() {
        cleanUp(this);
    }

}
