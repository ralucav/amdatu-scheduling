/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.scheduling.quartz.example.job;

import java.util.Properties;

import org.amdatu.scheduling.constants.Constants;
import org.amdatu.scheduling.quartz.example.api.MessageStore;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.quartz.Job;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		dm.add(createComponent().setInterface(MessageStore.class.getName(),
				null).setImplementation(MessageStoreImpl.class));

		dm.add(createComponent()
				.setInterface(Job.class.getName(), null)
				.setImplementation(ExampleJobAnnotations.class)
				.add(createServiceDependency().setService(MessageStore.class)
						.setRequired(true)));
		
		Properties properties = new Properties();
		properties.put(Constants.REPEAT_COUNT, new Integer(10));
		properties.put(Constants.REPEAT_INTERVAL_PERIOD, "millisecond");
		properties.put(Constants.REPEAT_INTERVAL_VALUE, new Long(50));
		dm.add(createComponent()
				.setInterface(Job.class.getName(), properties)
				.setImplementation(ExampleJobProperties.class)
				.add(createServiceDependency().setService(MessageStore.class)
						.setRequired(true)));
	
	}

}
