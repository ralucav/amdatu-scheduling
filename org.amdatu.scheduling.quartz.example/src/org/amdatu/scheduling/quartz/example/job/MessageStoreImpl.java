/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.scheduling.quartz.example.job;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import org.amdatu.scheduling.quartz.example.api.MessageStore;

public class MessageStoreImpl implements MessageStore {
	private Queue<String> messages = new ArrayBlockingQueue<>(20);
	
	@Override
	public void addMessage(String message) {
		messages.add(message);
	}

	@Override
	public long getNrOfMessages(String prefix) {
		return messages.stream().filter(m -> m.startsWith(prefix)).count();
	}

}
