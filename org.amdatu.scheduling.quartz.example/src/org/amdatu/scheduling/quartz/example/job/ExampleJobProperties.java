/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.scheduling.quartz.example.job;

import org.amdatu.scheduling.annotations.RepeatCount;
import org.amdatu.scheduling.quartz.example.api.MessageStore;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@RepeatCount(12)
public class ExampleJobProperties implements Job {
	private volatile MessageStore m_messageStore;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		m_messageStore.addMessage("[PROPERTIES JOB] Message at " + System.currentTimeMillis());

	}

}
